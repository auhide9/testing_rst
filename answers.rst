
16. What is the AWS View tab used for?
--------------------------------------

| `AWS View <https://dmpweb.securities.com/aws.html>`_ is a place in which you can see information about our AWS services.
| You can observe that there are different tables with information about AWS server instances for formatting, reception, WDP and so on.
| An interesting, not so self-explanatory thing, that you can check out, is when you put your cursor over the numbers under **RMfeeds**, 
| you can see the count of feeds, of a certain inbox, that are on hold because the formatting servers are full of data, and are not 
| capable of handling more processes.

.. image:: images/awsview.png

|
|

17. By what criteria do we choose which ticket to start working on?
-------------------------------------------------------------------
| Every ticket has a **priority level**, by which we can see the level of urgency for it to be resolved. 
| When we look at the Issue Feed, in the `JIRA Issue Navigator <https://securities.jira.com/issues>`_, 
| we can see on the rightmost side of the feed, the priorities of the issues/tickets:

.. image:: images/issues_feed.png
|

There are 6 priority levels:


.. image:: images/priorities.png

|
|

18. What are industry codes used for?
-------------------------------------
| The industry code is used for mapping the type of the industries to certain numbers(codes).
| `EMIS Search Page <https://www.emis.com/php/search/search>`_  - here you can filter out articles by a lot of things, and one of those things is by *Industry*.
| Firstly, you'll have to click on the **More Filters** button, then click on the dropdown - **Industries**.
| There you go, now you can see how the industries are linked to a certain combination of digits.

|
|

19. How to match special symbols in the processing parser script if the current one doesn't work?
----------------------------------------------------------------------------------------------------
|
For example, let's take into consideration this case:
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
| The string that is at fault for our broken script is - '**cómoda**'.
| This string has to be removed from all articles, but it seems that fully hardcoding it doesn't work.
| *As if we have to be aware of something else?*
|
| Here is how to do it:
.. code:: c
   
   my $re_problematic_string = 'c[^<]+moda';

   # Substituting the string with an empty one.
   $body =~ s/$re_problematic_string//ig;

|

| Explanation:
| Here the trick is that you have to be aware that sometimes, when a certain encoding doesn't recognize a symbol (in this case - ó),
| it reads it as many symbols, none of which is this one.
| So what we do here is, instead of matching one symbol, we match one or more, so that even if the symbol breaks, when it is being read,
| our script will still substitute it.

|
|

20. Why is it necessary, in some cases, to change the encoding of the data in the processing parser?
----------------------------------------------------------------------------------------------------
| Sometimes it is necessary, because depending on the language of the text that is coming as an input,
| at the final stage of the processing, our framework expects it to have a certain encoding.
| For example if the text is in Bulgarian, at the final step, before publishing, the framework will expect,
| let's say, CP-1251. But sometimes the input encoding won't match with the one that the framework deduced.
| What happens then is, the final product will have broken symbols. Which is not cool, isn't it?
| So what we can do about it, in the Processing Parser Tab, is, if we know what the encoding of the text
| coming as an input is, before the publishing, we can convert it from `input encoding` to `expected encoding`.
| Here the expected encoding of the framework, becomes the one that you've specified.
.. image:: images/enc_conv.png

|

| Another way to do it, if you are SURE that the incoming text is in UTF-8, you can change the expected encoding
| to be UTF-8, from the Processing Tab.
.. image:: images/proc_utf8.png